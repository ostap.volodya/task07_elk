package com.ostapiuk

import java.sql.Timestamp

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.from_json
import org.apache.spark.sql.types.{IntegerType, StringType, StructField, StructType}
import org.elasticsearch.hadoop.cfg.ConfigurationOptions

object KafkaToElasticSearch {
  val topic = "Ostapiuk"
  val port = "localhost:9092"
  val spark = SparkSession.builder()
    .config("spark.sql.warehouse.dir", "./spark-warehouse")
    .config(ConfigurationOptions.ES_NODES, "127.0.0.1")
    .config(ConfigurationOptions.ES_PORT, "9200")
    .config("es.index.auto.create", "true")
    .master("local[*]")
    .appName("elk")
    .getOrCreate()

  import spark.implicits._

  def main(args: Array[String]): Unit = {
    val schema = StructType(
      List(
        StructField("hotel_id", StringType, nullable = true),
        StructField("ShortCount", IntegerType, nullable = true),
        StructField("StandartCount", IntegerType, nullable = true),
        StructField("StandartExtCount", IntegerType, nullable = true),
        StructField("LongCount", IntegerType, nullable = true),
        StructField("ErrorCount", IntegerType, nullable = true),
        StructField("totalCount", IntegerType, nullable = true),
        StructField("WithChildCount", IntegerType, nullable = true),
        StructField("WithoutChildCount", IntegerType, nullable = true),
        StructField("totalChildCount", IntegerType, nullable = true)
      )
    )
    val output = spark
      .readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", port)
      .option("subscribe", topic)
      .load()

    val result = output.selectExpr("CAST(value AS STRING)", "CAST(timestamp AS TIMESTAMP)").as[(String, Timestamp)]
      .select(from_json($"value", schema).as("data"), $"timestamp")
      .select("data.*", "timestamp")

    result.writeStream
      .outputMode("append")
      .queryName("writing_to_es")
      .format("org.elasticsearch.spark.sql")
      .option("checkpointLocation", "./checkpoints")
      .option("es.resource", "hotel-index/_doc")
      .option("es.nodes", "localhost:9200")
      .start()
      .awaitTermination()
  }
}
